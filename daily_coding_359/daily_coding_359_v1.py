word = "threesevenfoursixtwo"
word_as_list = []

for x in word:
    word_as_list.append(x)

zero = {0: ['z', 'e', 'r', 'o']}
one = {1: ['o', 'n', 'e']}
two = {2: ['t', 'w', 'o']}
three = {3: ['t', 'h', 'r', 'e', 'e']}
four = {4: ['f', 'o', 'u', 'r']}
five = {5: ['f', 'i', 'v', 'e']}
six = {6: ['s', 'i', 'x']}
seven = {7: ['s', 'e', 'v', 'e', 'n']}
eight = {8: ['e', 'i', 'g', 'h', 't']}
nine = {9: ['n', 'i', 'n', 'e']}

possible_numbers = [zero, one, two, three, four, five, six, seven, eight, nine]

sorted_numbers = []


while word != '':
    reset_flag = False
    for possible_number in possible_numbers:

        for key, value in possible_number.items():
            included = True
            for letter in value:
                if letter in word:
                    continue
                else:
                    included = False
                    break

            if included:

                sorted_numbers.append(key)

                for letter in value:
                    word_as_list.remove(letter)

                new_word = ''
                for letter in word_as_list:
                    new_word = new_word + letter

                word = new_word
                word_as_list = []
                for x in word:
                    word_as_list.append(x)
                reset_flag = True
        if reset_flag:
            break

print(sorted_numbers)

given_list = [2, 1, 20, 1]
end = len(given_list) - 1


def reach_end(current_hops, acc_hops, counter):
    acc_hops += current_hops
    if acc_hops == end:
        return True
    elif acc_hops > end or counter > end:
        return False

    if current_hops == 0:
        return False
    next_hops = given_list[acc_hops]

    return reach_end(next_hops, acc_hops, counter + 1)


print(reach_end(given_list[0], 0, 0))

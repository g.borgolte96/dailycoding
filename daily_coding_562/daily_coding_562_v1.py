from copy import deepcopy
import math

given_list = [2, 2, 3, 4, 5]


def exclude_number_from_product(number):
    """
    Exclude given number from the product calculation
    """
    index = given_list.index(number)
    new_list = deepcopy(given_list)
    new_list.pop(index)

    product = math.prod(new_list)

    # for number in new_list:
    #     product = product * number

    return product


result = map(exclude_number_from_product, given_list)

print(list(result))
